;;-*-coding: utf-8;-*-
(define-abbrev-table 'ledger-mode-abbrev-table
  '(
    ("insertdate" "" insertdate :count 15)
   ))

(define-abbrev-table 'org-mode-abbrev-table
  '(
    ("evalrow" "" evalrow :count 6)
    ("initblock" "#+BEGIN_SRC emacs-lisp :tangle init.el

#+END_SRC" nil :count 8)
   ))

(define-abbrev-table 'text-mode-abbrev-table
  '(
    ("insertdate" "" insertdate :count 0)
   ))

