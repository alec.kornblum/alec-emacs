(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '(
     "2e7dc2838b7941ab9cabaa3b6793286e5134f583c04bde2fba2f4e20f2617cf7"
     "fbf73690320aa26f8daffdd1210ef234ed1b0c59f3d001f342b9c0bbf49f531c"
     "e410458d3e769c33e0865971deb6e8422457fad02bf51f7862fa180ccc42c032"
     "9a977ddae55e0e91c09952e96d614ae0be69727ea78ca145beea1aae01ac78d2"
     "36b57dcbe8262c52d3123ed30fa34e5ef6b355881674142162a8ca8e26124da9"
     "6b912e025527ffae0feb76217f1a3e494b0699e5219ab59ea4b3a36c319cea17"
     "b29ba9bfdb34d71ecf3322951425a73d825fb2c002434282d2e0e8c44fce8185"
     "a242356ae1aebe9f633974c0c29b10f3e00ec2bc96a61ff2cdad5ffa4264996d"
     "0f76f9e0af168197f4798aba5c5ef18e07c926f4e7676b95f2a13771355ce850"
     "1ea82e39d89b526e2266786886d1f0d3a3fa36c87480fad59d8fab3b03ef576e"
     "b5c3c59e2fff6877030996eadaa085a5645cc7597f8876e982eadc923f597aca"
     "aed3a896c4ea7cd7603f7a242fe2ab21f1539ab4934347e32b0070a83c9ece01"
     "8746b94181ba961ebd07c7397339d6a7160ee29c75ca1734aa3744274cbe0370"
     "21e3d55141186651571241c2ba3c665979d1e886f53b2e52411e9e96659132d4"
     "69f7e8101867cfac410e88140f8c51b4433b93680901bb0b52014144366a08c8"
     "a1b3cb714307bcf00b7ab40bbc5e404f9a690c2fff5485e4f7421a6e87502d40"
     default
     ))
 '(package-selected-packages
   '(
     aggressive-indent
     eglot-fsharp
     fsharp-mode
     sly-asdf
     sly-repl-ansi-color
     sly-quicklisp
     sly-named-readtables
     sly-macrostep
     sly
     python-mls
     yasnippet-snippets
     magit
     halloweenie-theme
     yasnippet
     breadcrumb
     elisp-demos
     avy
     drag-stuff
     ahk-mode
     emmet-mode
     web-mode
     web-beautify
     calibre
     cdlatex
     hy-mode
     litex-mode
     olivetti
     substitute
     xr
     ht
     gnuplot-mode
     gnuplot
     snow
     fireplace
     org-journal
     embark-consult
     marginalia
     embark
     consult
     git-timemachine
     corfu-doc
     eglot
     cape
     corfu
     nginx-mode
     orderless
     vertico
     paredit
     chemtable
     csv-mode
     org-contrib
     org-drill
     markdown-preview-mode
     smex
     deft
     org-roam
     ledger-mode
     blacken
     winum
     mu2tex
     helpful
     evil-surround
     ag
     projectile
     undo-fu
     elfeed
     elfeed-org
     modus-themes
     evil-org
     nov
     calibredb
     which-key
     bbdb
     evil
     denote
     ))
 '(safe-local-variable-values
   '((eval add-hook 'web-mode-hook #'org-src-tangle-mode nil t)
     (\#+org-attach-dir-relative . t)
     (org-enforce-todo-dependencies . t))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
